﻿//Norman Nguyen
//This is the Little Guy AI Controller
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(AIController))]
public class LittleAIController : MonoBehaviour
{
    public TankData data;
    public AIController controller;
    public enum AIStates
    {
        Idle, Flee
    }
    public AIStates currentState;
    public float timeInCurrentState;
    public float fleeDistance = 5;

    public float fleeTime = 10;
    Transform aiPosition;
    Transform playerPosition;
    // Use this for initialization
    void Start()
    {
        controller = GetComponent<AIController>();
    }
    // Update is called once per frame
    void Update()
    {
        timeInCurrentState += Time.deltaTime;
        switch (currentState)
        {
            //Idle State
            case AIStates.Idle:
                Idle();
                if (Vector3.Distance(data.motor.tf.position, GameManager.instance.player.data.motor.tf.position) < fleeDistance)
                {
                    ChangeState(AIStates.Flee);
                }
                break;
            //Flee State
            case AIStates.Flee:
                Flee();
                if (timeInCurrentState > fleeTime)
                {
                    ChangeState(AIStates.Idle);
                }
                break;
        }
    }
    //Change States
    public void ChangeState(AIStates newState)
    {
        currentState = newState;
        timeInCurrentState = 0;
    }
    //Leave it be
    void Idle()
    {

    }
    //Flee Method for the Little Tank
    void Flee()
    {
        Vector3 vectorToPlayer = GameManager.instance.player.data.motor.tf.position - data.motor.tf.position;
        vectorToPlayer = -1 * vectorToPlayer;
        //Back to Normal State
        vectorToPlayer.Normalize();
        controller.MoveTowards(vectorToPlayer);
    }
}
