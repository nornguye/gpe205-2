AI Tank Personalities

1. The Grunt (Patrol Tank)

-Well Balanced Tank with normal speed, fire, and attacks
-It chases the player if in plain sight
-Later goes back

2. The Little Guy (Coward Tank)
-Not known for attacks
-Known to evade and flee from the player.

3. Heavy Duty (Stationary Tank)
-Stationed
-Has sight and hearing
-Fires the player as it sees.


4. The Anger (Aggressive Tank)
-Chases the player and it fires.

