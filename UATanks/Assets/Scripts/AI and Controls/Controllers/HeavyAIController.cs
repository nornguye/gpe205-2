﻿//Norman Nguyen
//This is the Heavy AI Controller
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class HeavyAIController : MonoBehaviour
{
    public TankData data;
    public AIController controller;
    //Target the player tank
    private Transform target;
    //Put the tank firing point (Spawn bullets) on the AI Tank
    private TankCannon cannon;
    public float viewDistance = 10; //View Distance
    public float fieldOfView = 30.0f; //Your Field of View for the player.
    public float hearDistance = 10.0f; //Hear Distance
    public float attackTime = 10.0f; //Attack Time
    //Enum of States
    public enum AIStates
    {
        Idle, Attack, LookAround
    }
    public AIStates currentState;
    public float timeInCurrentState;
    Transform playerTransform;
    //AI Transform
    Transform aiTransform;
    // Use this for initialization
    void Start()
    {
        controller = GetComponent<AIController>();
        cannon = GetComponent<TankCannon>();
    }
    // Update is called once per frame
    void Update()
    {
        playerTransform = GameManager.instance.player.data.motor.transform;
        aiTransform = data.motor.transform;
        timeInCurrentState += Time.deltaTime;
        switch (currentState)
        {
            //Idle
            case AIStates.Idle:
                Idle();
                //Can Hear
                if (CanHear())
                {
                    ChangeState(AIStates.LookAround);
                }
                if (CanSee())
                {
                    ChangeState(AIStates.Attack);
                }
                break;
            //Look Around
            case AIStates.LookAround:
                data.motor.Turn(3); //Turns the tank
                //Sees the player will attack
                if (CanSee())
                {
                    ChangeState(AIStates.Attack);
                }
                break;
            //Attack
            case AIStates.Attack:
                Attack();
                //Back to Idle if it hits to the countdown limit (10).
                if (timeInCurrentState > attackTime)
                {
                    ChangeState(AIStates.Idle);
                }
                break;
        }
    }
    //Change State for the FSM
    void ChangeState(AIStates newState)
    {
        currentState = newState;
        timeInCurrentState = 0;
    }
    //IDLE = leave it be.
    void Idle()
    {

    }
    //Attack Method
    void Attack()
    {
        Vector3 relativePos = playerTransform.position - transform.position;
        Quaternion rotation = Quaternion.LookRotation(relativePos, Vector3.up);
        transform.rotation = rotation;
        FireAtPlayer();
    }
    //Fire at the player.
    void FireAtPlayer()
    {
        //Fire bullets at the player as it sees
        cannon.Fire();
    }
    //Can Hear the player
    bool CanHear()
    {
        float distance = Vector3.Distance(aiTransform.position, playerTransform.position);
        return distance < hearDistance;
    }
    //Can see the player
    bool CanSee()
    {
        //Vector to Target
        Vector3 vectorToTarget = playerTransform.transform.position - aiTransform.position;
        float angle = Vector3.Angle(vectorToTarget, aiTransform.forward);
        if (angle > fieldOfView)
        {
            return false;
        }
        //Raycast
        RaycastHit hitInfo;
        Physics.Raycast(aiTransform.position, vectorToTarget, out hitInfo, viewDistance);

        if (hitInfo.collider == null)
        {
            return false;
        }
        Collider targetCollider = GameManager.instance.player.data.GetComponent<Collider>();
        if (targetCollider != hitInfo.collider)
        {
            return false;
        }
        return true;
    }
}
