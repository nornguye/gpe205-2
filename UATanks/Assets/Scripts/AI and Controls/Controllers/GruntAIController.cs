﻿//Norman Nguyen
//This is the Grunt AI Controller
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GruntAIController : MonoBehaviour
{
    //Tank Data component
    public TankData data;
    //Main AI Controller component
    AIController controller;
    //Cannon Component
    private TankCannon cannon;
    //Enum list of AI States
    public enum AIStates
    {
        Chase, Patrol
    }
    public AIStates currentState;
    //Enum List for Patrol
    public float timeInCurrentState;
    public float chaseDistance = 5; //Chase Distance
    public float chaseTime = 10.0f; //Chase Time
    //Player Transform
    Transform playerTransform;
    //AI Transform
    Transform aiTransform;
    // Use this for initialization
    void Start()
    {
        //Get AI Controller Component
        controller = GetComponent<AIController>();
        cannon = GetComponent<TankCannon>();
    }

    // Update is called once per frame
    void Update()
    {
        //Current Time State match delta time.
        timeInCurrentState += Time.deltaTime;
        playerTransform = GameManager.instance.player.data.motor.transform;
        //AI Position
        aiTransform = data.motor.transform;

        switch (currentState)
        {
            case AIStates.Chase:
                Chase();
                FireAtPlayer();
                if (timeInCurrentState > chaseTime)
                {
                    ChangeState(AIStates.Patrol);
                }
                break;
            case AIStates.Patrol:
                controller.Patrol();
                if (Vector3.Distance(aiTransform.position, playerTransform.position) < chaseDistance)
                {
                    ChangeState(AIStates.Chase);
                }
                break;
        }
    }
    //Change AI State for the Grunt (Chase/Patrol)
    public void ChangeState(AIStates newState)
    {
        //Change your current state to a new state
        currentState = newState;
        timeInCurrentState = 0;
    }
    //Chase Method
    void Chase()
    {
        controller.MoveTowards(GameManager.instance.player.data.motor.tf.position);
    }
    //Patrol Method
    
    void FireAtPlayer()
    {
        //Fire bullets at the player as it sees
        cannon.Fire();
    }
}
