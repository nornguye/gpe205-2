﻿//Norman Nguyen
// Tank Turret Controls: This helps the turret to rotate within the mouse.

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankTurretControls : MonoBehaviour {
    //horizontal
    public float speedH = 2.0f;
    //yaw y
    private float yaw = 0.0f;
    //pitch x
    private float pitch = 0.0f;
    // Update on the turret movement where it focuses on the horizontal axis.
    void Update()
    {
        //using the Y-Axis for the Mouse X
        yaw += speedH * Input.GetAxis("Mouse X");
        //Euler angles helped with the x,y, and z axis, but we use the x-axis only
        transform.eulerAngles = new Vector3(pitch, yaw, 0.0f);

    }
}
